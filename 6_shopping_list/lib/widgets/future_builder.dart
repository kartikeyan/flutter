import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopping_list/data/categories.dart';
import 'package:shopping_list/models/grocery_item.dart';
import 'package:shopping_list/widgets/new_item.dart';
import 'package:http/http.dart'
    as http; // all content provided by this package should be bundeled into http object

class GroceryList extends StatefulWidget {
  const GroceryList({super.key});

  @override
  State<GroceryList> createState() => _GroceryListState();
}

class _GroceryListState extends State<GroceryList> {
  List<GroceryItem> _groceryItems = [];

  late Future<List<GroceryItem>> _loadedItems;

  @override
  void initState() {
    super.initState();
    _loadedItems = _loadItems();
  }

  Future<List<GroceryItem>> _loadItems() async {
    final url = Uri.https(
      "shoppinglist-9f2f6-default-rtdb.firebaseio.com", // first copying reference url

      // "abc.firebaseio.com", // for error checking

      'shopping-list.json', // path added, name can be anything
    );

    // for handling invalid domain, missing intenet connection
    final response = await http.get(url);

    //print(response.statusCode); // > 400 incicates problems

    if (response.statusCode >= 400) {
      throw Exception('Failed to fetch grocery items. Please try again..');
    }

    /*
    print(response); // Instance of 'Response'
    print(response
        .body); // {"-Nv0U6EPEZZMLSOaHO9m":{"category":"Hygiene","name":"kartik","quantity":10},"-Nv0X24C0j1v4KBWKpV1":{"category":"Dairy","name":"milk","quantity":12},"-Nv0Y7ULckPTg8aMS8hL":{"category":"Vegetables","name":"gg","quantity":1}}

    */

    if (response.body == 'null') {
      return [];
    }

    final Map<String, dynamic> listData = await json.decode(response.body);

    final List<GroceryItem> loadedItems = [];
    for (final item in listData.entries) {
      final category = categories.entries
          .firstWhere(
              (catItem) => catItem.value.title == item.value['category'])
          .value;

      loadedItems.add(
        GroceryItem(
          id: item.key,
          name: item.value['name'],
          quantity: item.value['quantity'],
          category: category,
        ),
      );
    }

    return loadedItems;
  }

  void _addItem() async {
    final newItem = await Navigator.of(context).push<GroceryItem>(
      MaterialPageRoute(
        builder: (context) => const NewItem(),
      ),
    );

    if (newItem == null) {
      return;
    }

    setState(() {
      _groceryItems.add(newItem);
    });
  }

  void _removeItem(GroceryItem item) async {
    final index = _groceryItems.indexOf(item);

    setState(() {
      _groceryItems.remove(item);
    });

    final url = Uri.https(
      "shoppinglist-9f2f6-default-rtdb.firebaseio.com", // first copying reference url

      //"abc.firebaseio.com", // for error checking

      'shopping-list/${item.id}.json', // path added, name can be anything
    );

    final response = await http.delete(
        url); // after deleting, in firebase deleted id will be shown for few seconds in red color

    if (response.statusCode >= 400) {
      // Optional ; Show Error message

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Failed to delete item. Please try again..'),
          duration: Duration(seconds: 3),
        ),
      );

      setState(() {
        _groceryItems.insert(index, item);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Your Groceries"),
        actions: [
          IconButton(
            onPressed: _addItem,
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: FutureBuilder(
        future: _loadedItems,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (snapshot.hasError) {
            return Center(
              child: Text(
                snapshot.error.toString(),
              ),
            );
          }

          if (snapshot.data!.isEmpty) {
            return const Center(
              child: Text("No items added yet.."),
            );
          }

          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (context, index) {
              return Dismissible(
                onDismissed: (direction) {
                  _removeItem(snapshot.data![index]);
                },
                key: ValueKey(snapshot.data![index].id),
                child: ListTile(
                  title: Text(snapshot.data![index].name),
                  leading: Container(
                    width: 24,
                    height: 24,
                    color: snapshot.data![index].category.color,
                  ),
                  trailing: Text(
                    snapshot.data![index].quantity.toString(),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
