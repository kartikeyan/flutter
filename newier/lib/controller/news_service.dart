import 'dart:convert';
import 'package:http/http.dart' as http;
import '../model/newsinfo_model.dart';

class NewsService {
  //final String apiKey = 'fe1e385dd8584b14ba46088b2115af61'; // API Key of Prasad
  final String apiKey = '5d45a65148d4449fbd871d85353da6fc';
  final String baseUrl = 'https://newsapi.org/v2';

  Future<List<Article>> searchNews(String query) async {
    final response = await http.get(
      Uri.parse('$baseUrl/everything?q=$query&apiKey=$apiKey'),
    );

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final List articles = data['articles'];
      return articles.map((json) => Article.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load search results');
    }
  }
}
