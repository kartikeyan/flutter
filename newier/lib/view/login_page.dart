import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newier/model/user_info_model.dart';
import 'package:newier/view/account_created.dart';
import 'package:newier/view/home.dart';

class LoginPageScreen extends StatefulWidget {
  const LoginPageScreen({super.key});

  @override
  State<LoginPageScreen> createState() => _LoginPageScreenState();
}

class _LoginPageScreenState extends State<LoginPageScreen> {
  final TextEditingController _nameLogInController = TextEditingController();
  final TextEditingController _passLogInController = TextEditingController();

  bool isPasswordSignInVisible = false;
  String enteredPassword = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _validateNextScreen() {
    bool loginValidated = _formKey.currentState!.validate();

    if (loginValidated) {
      String enteredUserName = _nameLogInController.text;
      enteredPassword = _passLogInController.text;

      bool matchingUser = userList.any(
        (element) =>
            element.userName == enteredUserName &&
            element.password == enteredPassword,
      );

      if (matchingUser) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Login Successful"),
            backgroundColor: Colors.green,
            duration: Duration(seconds: 1),
          ),
        );
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return const Home();
            },
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text(
              "Invalid UserName or Password",
            ),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 5),
            showCloseIcon: true,
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.only(left: 15, right: 10),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 90, top: 120),
                height: 200,
                width: 200,
                child: Image.asset("assets/images/login0.png"),
              ),
              Container(
                margin: const EdgeInsets.only(left: 105),
                child: Text(
                  "Welcome back",
                  style: GoogleFonts.poppins(
                    fontSize: 22,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                // height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(top: 30, left: 40, right: 40),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  controller: _nameLogInController,
                  decoration: const InputDecoration(
                    hintText: "Please enter your username",
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                    ),
                    prefixIcon: Icon(
                      Icons.person,
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Username";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                ),
              ),
              Container(
                // height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(top: 20, left: 40, right: 40),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  controller: _passLogInController,
                  obscureText: !isPasswordSignInVisible,
                  obscuringCharacter: "*",
                  decoration: InputDecoration(
                    hintText: "Please enter your password",
                    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    errorBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.key,
                    ),
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          isPasswordSignInVisible = !isPasswordSignInVisible;
                        });
                      },
                      child: Icon(
                        isPasswordSignInVisible
                            ? Icons.visibility_off
                            : Icons.remove_red_eye_outlined,
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Password";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 200, top: 10),
                child: Text(
                  "Forget password ?",
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    _validateNextScreen();
                  });
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.only(top: 30, left: 40, right: 40),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.blue.shade300,
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      "Login",
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 70, top: 30),
                    child: Text(
                      "Don't have an account ?",
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return const SignUpPage();
                          },
                        ),
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 5, top: 30),
                      child: Text(
                        "Sign Up",
                        style: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

List<NewUser> userList = [
  NewUser(userName: "kartik", password: "k123"),
  NewUser(userName: "onkar", password: "o123"),
  NewUser(userName: "prasad", password: "p123"),
];

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController _nameRegisterController = TextEditingController();
  final TextEditingController _passRegisterController = TextEditingController();
  final TextEditingController _confRegisterController = TextEditingController();

  bool passwordsMatch = false;
  bool isPasswordVisible = false;
  bool isConfPasswordVisible = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String? _validatePassword(String value) {
    if (value.isEmpty) {
      return "Please Enter Password";
    } else if (value != _passRegisterController.text) {
      setState(() {
        passwordsMatch = false;
      });
      return "Passwords do not Match";
    } else {
      // Passwords match
      setState(() {
        passwordsMatch = true;
      });

      return null;
    }
  }

  void _validateAndProceed() {
    if (_formKey.currentState!.validate()) {
      if (passwordsMatch) {
        setState(() {
          userList.add(NewUser(
              userName: _nameRegisterController.text,
              password: _passRegisterController.text));

          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text("User Successfully Registered"),
              backgroundColor: Colors.green,
              duration: Duration(seconds: 5),
              showCloseIcon: true,
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.only(left: 15, right: 10),
            ),
          );

          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return const AccountCreatedPage();
              },
            ),
          );
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Password does not match"),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 5),
            showCloseIcon: true,
            behavior: SnackBarBehavior.floating,
            margin: EdgeInsets.only(left: 15, right: 10),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 90, top: 120),
                height: 200,
                width: 200,
                child: Image.asset("assets/images/login1.png"),
              ),
              Container(
                margin: const EdgeInsets.only(left: 75),
                child: Text(
                  "Create New Account",
                  style: GoogleFonts.poppins(
                    fontSize: 22,
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                // height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(top: 30, left: 40, right: 40),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  controller: _nameRegisterController,
                  decoration: const InputDecoration(
                    hintText: "Please Enter Your Username",
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                    ),
                    prefixIcon: Icon(
                      Icons.person,
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Please Enter Username";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                ),
              ),
              Container(
                // height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(top: 20, left: 40, right: 40),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  controller: _passRegisterController,
                  obscureText: !isPasswordVisible,
                  obscuringCharacter: "*",
                  decoration: InputDecoration(
                    hintText: "Please Enter Your Password",
                    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    errorBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.key,
                    ),
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          isPasswordVisible = !isPasswordVisible;
                        });
                      },
                      child: Icon(
                        isPasswordVisible
                            ? Icons.visibility_off
                            : Icons.remove_red_eye_outlined,
                      ),
                    ),
                  ),
                  validator: (value) {
                    // print("In PasswordSignInVisible $value!");
                    // return _validatePassword(value!);    // should not be written
                    if (value == null || value.isEmpty) {
                      return "Please Enter Password";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              Container(
                // height: 50,
                width: double.infinity,
                margin: const EdgeInsets.only(top: 20, left: 40, right: 40),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  controller: _confRegisterController,
                  obscureText: !isConfPasswordVisible,
                  obscuringCharacter: "*",
                  decoration: InputDecoration(
                    hintText: "Confirm Your Password",
                    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    errorBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.key,
                    ),
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          isConfPasswordVisible = !isConfPasswordVisible;
                        });
                      },
                      child: Icon(
                        isConfPasswordVisible
                            ? Icons.visibility_off
                            : Icons.remove_red_eye_outlined,
                      ),
                    ),
                  ),
                  validator: (value) {
                    return _validatePassword(value!);
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    _validateAndProceed();
                  });
                },
                child: Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.only(top: 30, left: 40, right: 40),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.blue.shade300,
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      "Submit",
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 50, top: 30),
                    child: Text(
                      "Already have an account ?",
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return const LoginPageScreen();
                          },
                        ),
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 5, top: 30),
                      child: Text(
                        "Sign In",
                        style: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
