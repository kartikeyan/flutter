import 'package:flutter/material.dart';
import 'package:newier/model/newsinfo_model.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewWedview extends StatefulWidget {
  final Article article;
  const NewWedview({required this.article, super.key});

  @override
  State createState() => _NewWedviewState();
}

class _NewWedviewState extends State<NewWedview> {
  bool isPageLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "News updates",
          style: TextStyle(fontSize: 23, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: widget.article.url,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {},
            onPageStarted: (url) {
              setState(() {
                isPageLoading = true;
              });
            },
            onPageFinished: (progress) {
              setState(() {
                isPageLoading = false;
              });
            },
          ),
          isPageLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : const Stack(),
        ],
      ),
    );
  }
}
