import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:newier/view/aboutus.dart';
import 'package:newier/view/login_page.dart';
import 'package:newier/view/news_card.dart';
import 'package:newier/view/simpener.dart';
import '../controller/news_service.dart';
import '../model/news_list.dart';
import '../model/newsinfo_model.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share_plus/share_plus.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State createState() => _HomeState();
}

class _HomeState extends State {
  TextEditingController searchController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  String appbarName = "News Updates";
  int selectedDrawerButtonIndex = 0;
  bool _isShearPressed = false;
  bool _isAbouUsPressed = false;

  List lastSerch = [
    'india',
    'cricket',
    'ipl',
    'election',
    'weather',
    'bollywood',
    'trending',
    'election',
  ];

  @override
  void initState() {
    super.initState();
    NewsList.futureArticles = NewsService().searchNews("wold");
  }

  Future<void> _refresh() async {
    await Future.delayed(const Duration(milliseconds: 500));

    String searchKeyword = searchController.text.trim();
    if (searchKeyword == "") {
      searchKeyword = "wold";
    }
    NewsList.futureArticles = NewsService().searchNews(searchKeyword);
    setState(() {});
  }

  void _shareApp(BuildContext context) {
    const String text =
        "Link for the app Comming soon, still for apk massage on 7822028734 or \n insta:  https://www.instagram.com/kartikeyan_45/ ";
    Share.share(text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            appbarName,
            style: const TextStyle(fontSize: 23, fontWeight: FontWeight.w500),
          ),
          centerTitle: true,
        ),
        body: RefreshIndicator(
          onRefresh: _refresh,
          child: FutureBuilder<List<Article>>(
            future: NewsList.futureArticles,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return ListView.builder(
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        (index == 0) ? mySerchbar() : const SizedBox(),
                        const ShimmerNewsCard(),
                      ],
                    );
                  },
                );
              } else if (snapshot.hasError) {
                return ListView.builder(
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          mySerchbar(),
                          Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.25,
                                ),
                                const Text(
                                  "Oops !",
                                  style: TextStyle(
                                      fontSize: 21,
                                      fontWeight: FontWeight.w500),
                                  textAlign: TextAlign.center,
                                ),
                                const Text(
                                  " Slow or No internet connection",
                                  style: TextStyle(
                                      fontSize: 19,
                                      fontWeight: FontWeight.w400),
                                  textAlign: TextAlign.center,
                                ),
                                Image.asset(
                                  "assets/images/nointernet.jpg",
                                  height: 75,
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    });
              } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                return ListView.builder(
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          mySerchbar(),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.26,
                              ),
                              const Text(
                                'No articles found,\n Please enter proper keyword',
                                style: TextStyle(fontSize: 18),
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Image.asset(
                                "assets/images/noarticalfound.png",
                                height: 75,
                              ),
                            ],
                          ),
                        ],
                      );
                    });
              } else {
                return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    final article = snapshot.data![index];
                    try {
                      article.urlToImage!;
                    } catch (e) {
                      article.urlToImage =
                          "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Placeholder_view_vector.svg/991px-Placeholder_view_vector.svg.png";
                    }
                    try {
                      article.description!;
                    } catch (e) {
                      article.description = "No description avaliable";
                    }

                    return Column(
                      children: [
                        (index == 0) ? mySerchbar() : const SizedBox(),
                        (article.title != "[Removed]")
                            ? NewsCard(article: article)
                            : const SizedBox(),
                      ],
                    );
                  },
                );
              }
            },
          ),
        ),
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.03,
                      ),
                      const Text(
                        "Newier",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const Text(
                        "See all news update here",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 0;
                    Navigator.of(context).pop();
                    NewsList.futureArticles = NewsService().searchNews("india");
                    searchController.text = "";
                    appbarName = "News Updates";

                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.home,
                    label: "Home",
                    buttonIndex: 0,
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 1;
                    Navigator.of(context).pop();
                    NewsList.futureArticles =
                        NewsService().searchNews("Artificial Intelligence");
                    searchController.text = "Artificial Intelligence";
                    appbarName = "Artificial Intelligence";

                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.computer_sharp,
                    label: "AI (ARTIFICIAL INTELLIGENCE)",
                    buttonIndex: 1,
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 2;

                    Navigator.of(context).pop();
                    NewsList.futureArticles =
                        NewsService().searchNews("startup");
                    searchController.text = "Startup";
                    appbarName = "Startup";

                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.stairs_outlined,
                    label: "STARTUP",
                    buttonIndex: 2,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 3;
                    setState(() {});
                    Navigator.of(context).pop();
                    NewsList.futureArticles =
                        NewsService().searchNews("stockmarket");
                    searchController.text = "Stockmarket";
                    appbarName = "Stockmarket";
                  },
                  child: getDrawerButtons(
                    icon: Icons.analytics,
                    label: "STOCK MARKET",
                    buttonIndex: 3,
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 4;
                    Navigator.of(context).pop();
                    NewsList.futureArticles =
                        NewsService().searchNews("international");
                    searchController.text = "International";
                    appbarName = "International";
                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.flag,
                    label: "INTERNATIONAL",
                    buttonIndex: 4,
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 5;
                    Navigator.of(context).pop();
                    NewsList.futureArticles =
                        NewsService().searchNews("cricket football");
                    searchController.text = "Sports";
                    appbarName = "Sports";

                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.sports_cricket,
                    label: "SPORTS",
                    buttonIndex: 5,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 6;
                    Navigator.of(context).pop();

                    NewsList.futureArticles =
                        NewsService().searchNews("business");
                    searchController.text = "Business";
                    appbarName = "Business";

                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.business,
                    label: "BUSINESS",
                    buttonIndex: 6,
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                GestureDetector(
                  onTap: () {
                    selectedDrawerButtonIndex = 7;
                    Navigator.of(context).pop();
                    NewsList.futureArticles =
                        NewsService().searchNews("economics");
                    searchController.text = "Economics";
                    appbarName = "Economics";

                    setState(() {});
                  },
                  child: getDrawerButtons(
                    icon: Icons.money,
                    label: "ECONOMICS",
                    buttonIndex: 7,
                  ),
                ),
                Container(
                  height: 1.5,
                  width: 270,
                  color: Colors.black12,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                ),
                GestureDetector(
                  onTapDown: (TapDownDetails details) {
                    setState(() {
                      _isShearPressed = true;
                    });
                  },
                  onTapUp: (TapUpDetails details) {
                    setState(() {
                      _isShearPressed = false;
                    });
                  },
                  onTap: () {
                    _shareApp(context);
                    setState(() {});
                  },
                  child: Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: _isShearPressed
                          ? const Color.fromARGB(39, 24, 170, 248)
                          : null,
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    //height: ,
                    //width: 186,
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.share,
                          color: Color.fromARGB(255, 31, 175, 253),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.02,
                        ),
                        Text(
                          "Share",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: (8 == selectedDrawerButtonIndex)
                                ? const Color.fromARGB(255, 42, 134, 221)
                                : null,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                GestureDetector(
                  onTapDown: (TapDownDetails details) {
                    setState(() {
                      _isAbouUsPressed = true;
                    });
                  },
                  onTapUp: (TapUpDetails details) {
                    setState(() {
                      _isAbouUsPressed = false;
                    });
                  },
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const AboutUs();
                    }));
                  },
                  child: Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: _isAbouUsPressed
                          ? const Color.fromARGB(39, 24, 170, 248)
                          : null,
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.group,
                          color: Color.fromARGB(255, 31, 175, 253),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.02,
                        ),
                        Text(
                          "About us",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: (8 == selectedDrawerButtonIndex)
                                ? const Color.fromARGB(255, 42, 134, 221)
                                : null,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _showLogoutConfirmationDialog(context);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: _isAbouUsPressed
                          ? const Color.fromARGB(39, 24, 170, 248)
                          : null,
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.logout,
                          color: Color.fromARGB(255, 31, 175, 253),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.02,
                        ),
                        Text(
                          "Logout",
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: (8 == selectedDrawerButtonIndex)
                                ? const Color.fromARGB(255, 42, 134, 221)
                                : null,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding:
                      const EdgeInsets.only(bottom: 50, right: 100, left: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          _launchApp(
                            url: "https://www.instagram.com/kartikeyan_45/",
                          );
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 86, 78, 58),
                            borderRadius: BorderRadius.all(
                              Radius.circular(25),
                            ),
                          ),
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(60),
                            ),
                            child: Image.asset("assets/images/insta_logo.jpg"),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _launchApp(
                            url:
                                "https://www.youtube.com/channel/UCQRjYSyYW-8Pl9P1zX9RSRw",
                          );
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 189, 7, 7),
                            borderRadius: BorderRadius.all(
                              Radius.circular(25),
                            ),
                          ),
                          child: ClipRRect(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(60)),
                              child: Image.asset("assets/images/yt_logo.png")),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _launchApp(
                            url: "https://www.linkedin.com/in/kartikeyan45/",
                          );
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 7, 85, 255),
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                          ),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(60)),
                            child:
                                Image.asset("assets/images/linkdin_logo.png"),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          final Uri whatsappUri = Uri(
                            scheme: 'https',
                            host: 'wa.me',
                            path: '/+917822028734',
                          );
                          _launchApp(url: whatsappUri.toString());
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 59, 161, 224),
                            borderRadius: BorderRadius.all(
                              Radius.circular(25),
                            ),
                          ),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(60)),
                            child:
                                Image.asset("assets/images/whatapp_logo.png"),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget mySerchbar() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 5),
              width: MediaQuery.of(context).size.width * 0.75,
              height: MediaQuery.of(context).size.height * 0.06,
              child: TextFormField(
                controller: searchController,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: const Color.fromARGB(255, 236, 236, 236),
                  hintText: 'eg. science',
                  hintStyle: const TextStyle(fontSize: 16),
                  prefixIcon: const Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    borderSide: const BorderSide(
                      color: Color.fromARGB(255, 224, 224, 224),
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    borderSide: const BorderSide(
                      color: Color.fromARGB(255, 255, 255, 255),
                      width: 2.0,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    borderSide: const BorderSide(
                      color: Color.fromARGB(255, 101, 100, 100),
                      width: 1.3,
                    ),
                  ),
                ),
                keyboardType: TextInputType.name,
                textInputAction: TextInputAction.done,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(
              width: 6,
            ),
            GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(_focusNode);
                searchNews();
              },
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width * 0.18,
                height: MediaQuery.of(context).size.height * 0.055,
                decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 77, 175, 255),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
                child: const Text(
                  "Search",
                  style: TextStyle(
                      fontSize: 16.5,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                ),
              ),
            )
          ],
        ),
        const SizedBox(
          height: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 40,
              width: MediaQuery.of(context).size.width - 5,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: lastSerch.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      log("ppp");
                      searchController.text = lastSerch[index];
                      NewsList.futureArticles =
                          NewsService().searchNews(lastSerch[index]);
                      setState(() {});
                    },
                    child: LastSearchCard(
                      searchKeyword: lastSerch[index],
                    ),
                  );
                },
              ),
            ),
          ],
        )
      ],
    );
  }

  void searchNews() {
    String searchKeyword = searchController.text.trim();
    if (searchKeyword.isNotEmpty) {
      NewsList.futureArticles = NewsService().searchNews(searchKeyword);
    } else {
      NewsList.futureArticles = NewsService().searchNews("science");
      searchController.text = "science";
    }
    setState(() {});
  }

  Widget getDrawerButtons({
    required IconData icon,
    required String label,
    required int buttonIndex,
  }) {
    return Container(
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: (buttonIndex == selectedDrawerButtonIndex)
            ? const Color.fromARGB(39, 24, 170, 248)
            : null,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      //height: ,
      //width: 186,
      alignment: Alignment.center,
      child: Row(
        children: [
          Icon(
            icon,
            color: const Color.fromARGB(255, 31, 175, 253),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.02,
          ),
          Text(
            label,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: (buttonIndex == selectedDrawerButtonIndex)
                  ? const Color.fromARGB(255, 42, 134, 221)
                  : null,
            ),
          )
        ],
      ),
    );
  }

  void _launchApp({required String url}) async {
    const fallbackUrl = "https://www.instagram.com/kartikeyan_45/";

    try {
      bool launched =
          await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }

  void _showLogoutConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Logout Confirmation'),
          content: const Text(
            'Are you sure you want to logout?',
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Cancel'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();

                    _logout();
                  },
                  child: const Text('Logout'),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  void _logout() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) {
          return const LoginPageScreen();
        },
      ),
    );
  }
}

class LastSearchCard extends StatefulWidget {
  final String searchKeyword;
  const LastSearchCard({super.key, required this.searchKeyword});

  @override
  State createState() => _LastSearchState();
}

class _LastSearchState extends State<LastSearchCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.all(5),
      decoration: const BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Text(widget.searchKeyword),
    );
  }
}
