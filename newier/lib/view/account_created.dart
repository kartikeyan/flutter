import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:newier/view/login_page.dart';

class AccountCreatedPage extends StatelessWidget {
  const AccountCreatedPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 150,
            width: 120,
            margin: const EdgeInsets.only(left: 135, top: 100),
            child: Image.asset(
              "assets/images/check.png",
              alignment: Alignment.center,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              left: 100,
              top: 10,
            ),
            child: Text(
              "Account Created",
              style: GoogleFonts.poppins(
                fontSize: 22,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            height: 250,
            width: double.infinity,
            margin: const EdgeInsets.only(left: 25, right: 25, top: 25),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.blue.shade100,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 50, top: 15),
                  child: Text(
                    "Welcome to Newier",
                    style: GoogleFonts.poppins(
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 15, top: 10, right: 10),
                  child: Text(
                    "Hello! Stay Updated with the latest news and explore various technologies tailored to your interests. Customize your feed, save articles and share with friends.",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 15, top: 20, right: 10),
                  child: Text(
                    "Enjoy your reading!",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 15, top: 20, right: 10),
                  child: Text(
                    "Best Regards,",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 15, right: 10),
                  child: Text(
                    "The Newier Team",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return const LoginPageScreen();
                },
              ));
            },
            child: Container(
              height: 50,
              width: double.infinity,
              margin: const EdgeInsets.only(top: 30, left: 40, right: 40),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.greenAccent.shade700,
              ),
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  "Continue",
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
